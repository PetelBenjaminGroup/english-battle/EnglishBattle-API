import request from 'supertest';
import { app } from './server';
import * as VerbService from './verbs/verbService';

describe('Main routing', () => {


    beforeAll(() => {
        VerbService.initVerbs();
    });

    it('/verbs - get', async () => {
        const res = await request(app).get('/api/verbs');
        expect(res.status).not.toBe(404);
    });
});