export interface Verb {
    baseVerbal: string,
    simplePast: string,
    pastParticipe: string,
    isRegular: boolean
}

export interface VerbQuery {
    type: VerbType,
    search: string
}

const verbType = ['both', 'regular', 'irregular'];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isVerbType = (x: any): x is VerbType => verbType.includes(x);

export type VerbType = (typeof verbType)[number];