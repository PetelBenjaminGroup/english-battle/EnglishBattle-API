import { isVerbType } from './verb.model';

describe('verb model', () => {
    describe('#isVerbType', () => {
        it('should return true', () => {
            expect(isVerbType('both')).toBe(true);
        });

        it('should return true', () => {
            expect(isVerbType('regular')).toBe(true);
        });

        it('should return true', () => {
            expect(isVerbType('irregular')).toBe(true);
        });

        it('should return false', () => {
            expect(isVerbType('wrong')).toBe(false);
        });
    });
});