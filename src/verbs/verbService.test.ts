import { Verb } from '../model/verb.model';
import * as verbService from'./verbService';

describe('VerbService', () => {
    it('Integration: initVerbs + _CSVToArray', () => {
        verbService.initVerbs();
        const verbs: Verb[] = verbService.getVerbs({ type: 'both', search: ''});
        const expected: Verb = { baseVerbal: 'go', simplePast: 'went', pastParticipe: 'gone', isRegular: false };
        const found = verbs.find(el => el.baseVerbal === 'go');

        expect(verbs).toBeDefined();
        expect(Array.isArray(verbs)).toBe(true);
        expect(verbs.length).toBeGreaterThan(900);
        expect(found).toBeDefined();
        expect(found?.baseVerbal).toBe(expected.baseVerbal);
        expect(found?.simplePast).toBe(expected.simplePast);
        expect(found?.pastParticipe).toBe(expected.pastParticipe);
        expect(found?.isRegular).toBe(expected.isRegular);
    });

    describe('#getVerbs', () => {

        beforeAll(() => {
            verbService.initVerbs();
        });

        it('should return all the verbs', () => {
            const results = verbService.getVerbs({ type: 'both', search: ''});

            expect(results).toBeDefined();
            expect(Array.isArray(results)).toBe(true);
            expect(results.length).toBeGreaterThan(900);
        });

        it('should return all the regular verbs', () => {
            const results = verbService.getVerbs({ type: 'regular', search: ''});

            expect(results).toBeDefined();
            expect(Array.isArray(results)).toBe(true);
            expect(results.length).toBeGreaterThan(850);
            expect(results.length).toBeLessThan(870);
            expect(results.findIndex(verb => verb.baseVerbal === 'waste')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'go')).toBe(-1);
        });

        it('should return all the iregular verbs', () => {
            const results = verbService.getVerbs({ type: 'irregular', search: ''});

            expect(results).toBeDefined();
            expect(Array.isArray(results)).toBe(true);
            expect(results.length).toBeGreaterThan(120);
            expect(results.length).toBeLessThan(140);
            expect(results.findIndex(verb => verb.baseVerbal === 'waste')).toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'go')).not.toBe(-1);
        });

        it('should return an empty list of verbs', () => {
            const results = verbService.getVerbs({ type: 'both', search: 'toto'});

            expect(results).toBeDefined();
            expect(results.length).toBe(0);
        });

        it('should return all the verbs with search', () => {
            const results = verbService.getVerbs({ type: 'both', search: 'cOme'});

            expect(results.length).toBe(4);
            expect(results.findIndex(verb => verb.baseVerbal === 'become')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'come')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'welcome')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'overcome')).not.toBe(-1);
        });

        it('should return all the regular search verbs', () => {
            const results = verbService.getVerbs({ type: 'regular', search: 'come'});

            expect(results.length).toBe(1);
            expect(results.findIndex(verb => verb.baseVerbal === 'come')).toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'welcome')).not.toBe(-1);
        });

        it('should return all the irregular search verbs', () => {
            const results = verbService.getVerbs({ type: 'irregular', search: 'come'});

            expect(results.length).toBe(3);
            expect(results.findIndex(verb => verb.baseVerbal === 'come')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'become')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'overcome')).not.toBe(-1);
            expect(results.findIndex(verb => verb.baseVerbal === 'welcome')).toBe(-1);
        });
    });
});