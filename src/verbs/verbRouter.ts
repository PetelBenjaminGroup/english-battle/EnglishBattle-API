import express, { Request, Response, Router } from 'express';
import { VerbQuery, isVerbType } from '../model/verb.model';
import * as VerbService from './verbService';

export const verbRouter: Router = express.Router();

verbRouter.get('/', (req: Request<unknown, unknown, unknown, VerbQuery>, res: Response) => {
    const search: string = req.query.search.toLowerCase();
    const type = req.query.type.toLowerCase();
    if(!isVerbType(type)) {
        throw new Error('Wrong type of verb. See documentation to get the rigth type');
    }   
    const verbs = VerbService.getVerbs({ search, type });
    res.json(verbs);
});