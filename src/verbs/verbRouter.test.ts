import request from 'supertest';
import express from 'express';
import { mocked } from 'jest-mock';
import { verbRouter } from './verbRouter';
import { getVerbs } from './verbService';
import { Verb, VerbQuery } from '../model/verb.model';

const mockVerbServiceGetVerbs: Verb[] = [{ baseVerbal: 'go', simplePast: 'went', pastParticipe: 'gone', isRegular: true }];

jest.mock('./verbService', () => {
    return {
        getVerbs: jest.fn().mockImplementation(() => mockVerbServiceGetVerbs)
    };
});

describe('verbRouter', () => {
    const app = express();

    beforeAll(() => {
        app.use('/', verbRouter);
    });
    describe('#get', () => {
        it('should reponse 500 without any params', (done) => {
            mocked(getVerbs);
            request(app)
                .get('/')
                .expect(500)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it('should reponse 500 without search param', (done) => {
            mocked(getVerbs);
            request(app)
                .get('/')
                .query({ type: 'both' })
                .expect(500)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }
                    expect(getVerbs).not.toBeCalled();
                    done();
                }); 
        });

        it('should reponse 500 without type param', (done) => {
            mocked(getVerbs);
            request(app)
                .get('/')
                .query({ search: 'come' })
                .expect(500)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }
                    expect(getVerbs).not.toBeCalled();
                    done();
                }); 
        });

        it('should reponse 500 with wrong type param', (done) => {
            mocked(getVerbs);
            request(app)
                .get('/')
                .query({ search: 'come', type: 'wrongtype' })
                .expect(500)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }
                    expect(getVerbs).not.toBeCalled();
                    done();
                }); 
        });
    
        it('with empty search query', (done) => {
            mocked(getVerbs);
            const queryParams: VerbQuery = { type: 'both', search:'' };
            request(app)
                .get('/')
                .query(queryParams)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(getVerbs).toBeCalledWith(queryParams);
                    expect(res.body).toEqual(expect.arrayContaining(mockVerbServiceGetVerbs));
                    done();
                });
        });
    
        it('with search query', (done) => {
            mocked(getVerbs);
            const queryParams = { type: 'rEgular', search: 'cOme' };
            request(app)
                .get('/')
                .query(queryParams)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(getVerbs).toBeCalledWith(
                        { type: queryParams.type.toLowerCase(), search: queryParams.search.toLowerCase() }
                    );
                    expect(res.body).toEqual(expect.arrayContaining(mockVerbServiceGetVerbs));
                    done();
                });
        });
    });
});