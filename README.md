# EnglishBattleAPI

## API

### /verbs

    **return:** a list of verbs
    **request params:**
        - search: (mandatory) the search verbs. If the string is empty, return all verbs
        - type: (mandatory) the type of search verb. * both / regular / irregular *. All other type will result in a 503
